/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.models;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import funding.custom.annotaions.DocumentName;

@DocumentName(collection = "Funding")
public class Funding extends Document<Funding> {
	private static final long serialVersionUID = 1L;
	@NotNull(message = "ammount should not be empty")
	private Double credit;
	@JsonIgnore
	private Double ledgerBalance;
	@JsonIgnore
	private Double clearBalance;
	private Long creditedOn;
	private String description;

	public Funding() {
	}

	public Double getLedgerBalance() {
		return ledgerBalance;
	}

	public void setLedgerBalance(Double ledgerBalance) {
		this.ledgerBalance = ledgerBalance;
	}

	public Double getClearBalance() {
		return clearBalance;
	}

	public void setClearBalance(Double clearBalance) {
		this.clearBalance = clearBalance;
	}

	public Double getCredit() {
		return credit;
	}

	public void setCredit(Double credit) {
		this.credit = credit;
	}

	public Long getCreditedOn() {
		return creditedOn;
	}

	public void setCreditedOn(Long creditedOn) {
		this.creditedOn = creditedOn;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}