/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.models;

import java.util.List;

import javax.validation.constraints.NotNull;

import funding.custom.annotaions.DocumentName;

@DocumentName(collection = "fundAccount")
public class FundAccount extends Document<FundAccount> {
	private static final long serialVersionUID = 1L;
	@NotNull(message = "name should not be empty")
	private String accountName;
	private List<String> userId;

	
	public List<String> getUserId() {
		return userId;
	}

	public void setUserId(List<String> userId) {
		this.userId = userId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public FundAccount() {
	}

}