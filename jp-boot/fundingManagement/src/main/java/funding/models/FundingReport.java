/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.models;

import java.util.List;

public class FundingReport extends Document<FundingReport> {
	private static final long serialVersionUID = 1L;

	private String reportName;
	private String from;
	private String to;
	private double totalAmountCredited;
	private double totalAmountDebited;
	private double balanceAmount;
	private List<Credit> credit;
	private List<Debit> debit;

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getfrom() {
		return from;
	}

	public void setfrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public double getTotalAmountCredited() {
		return totalAmountCredited;
	}

	public void setTotalAmountCredited(double totalAmountCredited) {
		this.totalAmountCredited = totalAmountCredited;
	}

	public double getTotalAmountDebited() {
		return totalAmountDebited;
	}

	public void setTotalAmountDebited(double totalAmountDebited) {
		this.totalAmountDebited = totalAmountDebited;
	}

	public double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public List<Credit> getCredit() {
		return credit;
	}

	public void setCredit(List<Credit> credit) {
		this.credit = credit;
	}

	public List<Debit> getDebit() {
		return debit;
	}

	public void setDebit(List<Debit> debit) {
		this.debit = debit;
	}

	public static class Credit {
		private String categoryName;
		private double amount;
		private double percentage;

		public String getCategoryName() {
			return categoryName;
		}

		public void setCategoryName(String categoryName) {
			this.categoryName = categoryName;
		}

		public double getAmount() {
			return amount;
		}

		public void setAmount(double amount) {
			this.amount = amount;
		}

		public double getPercentage() {
			return percentage;
		}

		public void setPercentage(double percentage) {
			this.percentage = percentage;
		}

	}

	public static class Debit {
		private String categoryName;
		private double amount;
		private double percentage;

		public String getCategoryName() {
			return categoryName;
		}

		public void setCategoryName(String categoryName) {
			this.categoryName = categoryName;
		}

		public double getAmount() {
			return amount;
		}

		public void setAmount(double amount) {
			this.amount = amount;
		}

		public double getPercentage() {
			return percentage;
		}

		public void setPercentage(double percentage) {
			this.percentage = percentage;
		}

	}

	public FundingReport() {
	}
}