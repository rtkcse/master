/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.models;

import javax.validation.constraints.NotNull;

import funding.custom.annotaions.DocumentName;
@DocumentName(collection = "creditCategory")
public class CreditCategory extends Document<CreditCategory>{
	private static final long serialVersionUID = 1L;
	@NotNull(message = "name should not be empty")
	private String name;
	private String accountId;
	public CreditCategory() {
	}

	public String getName() {
		return name;
	}

	public String getAccountId() {
		return accountId;
	}


	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}


	public void setName(String name) {
		this.name = name;
	}

}