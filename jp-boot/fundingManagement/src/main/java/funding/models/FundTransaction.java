/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.models;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import funding.custom.annotaions.DocumentName;

@DocumentName(collection = "fundTransaction")
public class FundTransaction extends Document<FundTransaction> {
	private static final long serialVersionUID = 1L;
	@NotNull(message = "fundAccountId should not be empty")
	private String fundAccountId;
	private String categoryID;
	private String transferedBy;
	@NotNull(message = "amount should not be empty")
	private Double amount;
	private long transactionDate;
	@NotNull(message = "title should not be empty")
	private String title;
	private String description;
	private String versionId;
	private String fileKey;
	@NotNull(message = "transactionType should not be empty")
	private FundTransactionType transactionType;
	public enum FundTransactionType {
		CR, DR
	}
	
	
	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public String getFileKey() {
		return fileKey;
	}

	public void setFileKey(String fileKey) {
		this.fileKey = fileKey;
	}

	public FundTransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(FundTransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public String getFundAccountId() {
		return fundAccountId;
	}

	public void setFundAccountId(String fundAccountId) {
		this.fundAccountId = fundAccountId;
	}

	public String getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(String categoryID) {
		this.categoryID = categoryID;
	}

	public String getTransferedBy() {
		return transferedBy;
	}

	public void setTransferedBy(String transferedBy) {
		this.transferedBy = transferedBy;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public long getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(long transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}