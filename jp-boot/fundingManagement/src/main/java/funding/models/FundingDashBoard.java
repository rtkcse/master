/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.models;

import funding.custom.annotaions.DocumentName;

@DocumentName(collection = "FundingDashBoard")
public class FundingDashBoard extends Document<FundingDashBoard> {
	private static final long serialVersionUID = 1L;
	
	private Double clearBalance;

	public FundingDashBoard() {
	}

	public Double getClearBalance() {
		return clearBalance;
	}

	public void setClearBalance(Double clearBalance) {
		this.clearBalance = clearBalance;
	}

	
}