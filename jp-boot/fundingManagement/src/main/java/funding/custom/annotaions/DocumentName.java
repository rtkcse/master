/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.custom.annotaions;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;
import org.springframework.data.annotation.Persistent;


@Persistent
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface DocumentName {


	/**
	 * The collection the document representing the entity is supposed to be stored in. If not configured, a default
	 * collection name will be derived from the type's name. The attribute supports SpEL expressions to dynamically
	 * calculate the collection to based on a per operation basis.
	 * 
	 * @return the name of the collection to be used.
	 */
	@AliasFor("collection")
	String value() default "";

	/**
	 * The collection the document representing the entity is supposed to be stored in. If not configured, a default
	 * collection name will be derived from the type's name. The attribute supports SpEL expressions to dynamically
	 * calculate the collection to based on a per operation basis.
	 * 
	 * @return the name of the collection to be used.
	 */
	@AliasFor("value")
	String collection() default "";

	/**
	 * Defines the default language to be used with this document.
	 *
	 * @return
	 * @since 1.6
	 */
	String language() default "";

	/**
	 * Defines the collation to apply when executing a query or creating indexes.
	 *
	 * @return an empty {@link String} by default.
	 * @since 2.2
	 */
	String collation() default "";

}

