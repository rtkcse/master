/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.dao;

import java.util.List;

import funding.common.Query;
import funding.models.Document;

public interface CommonRepository<T extends Document> {
	 <T> T create(Document document);

	 <T> T update(String id, Document document);
	
	 <T> List<T> get(Query querys, Class<T> documentClass);
	
	 <T> T getById(String id, funding.common.Query querys, Class<T> documentClass);

	void delete(String id, Class<T> documentClass);



}
