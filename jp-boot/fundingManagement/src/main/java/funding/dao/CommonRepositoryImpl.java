/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.dao;

import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import funding.common.Query.ORDER;
import funding.common.impl.QueryImpl;
import funding.models.Document;
import funding.models.Document.DELETE;

@Repository
public class CommonRepositoryImpl<T extends Document> implements CommonRepository<Document> {

	private static final String ID = "_id";

	@Autowired
	private MongoTemplate mongoTemplate;

	@SuppressWarnings("unchecked")
	@Override
	public <T> T create(Document document) {
		document.setDel(DELETE.N);
		return (T) this.mongoTemplate.save(document);
	}
	@SuppressWarnings("unchecked")
	@Override
	public <T> T update(String id, Document document) {
		document.setId(id);
		document.setDel(DELETE.N);
		return (T) this.mongoTemplate.save(document);
	}

	@Override
	public <T> List<T> get(funding.common.Query querys, Class<T> documentClass) {
		Query query = new Query();
		query = this.toFilter(querys).with(toOrder(querys)).skip(querys.getStart()).limit(querys.getCount());
		query = this.valueToReturns(querys, query);
		return (List<T>) mongoTemplate.find(query, documentClass);
	}

	private Query valueToReturns(funding.common.Query querys, Query query) {
		querys.getKeysToReturn().stream().forEach(key -> {
			query.fields().include(key);
		});
		return query;
	}

	@Override
	public <T> T getById(String id, funding.common.Query querys, Class<T> documentClass) {
		Query query = new Query();
		query = this.valueToReturn(querys).addCriteria(toMongoId(id));
		return mongoTemplate.findOne(query, documentClass);
	}

	@Override
	public void delete(String id, Class<Document> documentClass) {
		Document currentDocument = (Document) this.getById(id, QueryImpl.builder().build(), documentClass);
		currentDocument.setDel(DELETE.Y);
		this.mongoTemplate.save(currentDocument);
	}

	private Sort toOrder(funding.common.Query querys) {
		if (querys.getSortOrder() != null) {
			if (querys.getSortOrder().equals(ORDER.ASC)) {
				return Sort.by(Direction.ASC, querys.getSortBy());
			} else {
				return Sort.by(Direction.DESC, querys.getSortBy());
			}
		} else {
			return Sort.by(Direction.ASC, ID);
		}

	}

	private Criteria toMongoId(String id) {
		return Criteria.where(ID).is(id);
	}

	private Query toFilter(funding.common.Query querys) {
		Query query = new Query();
		Set<Entry<String, Object>> filterQuery = querys.getFilters().entrySet();
		filterQuery.forEach(each -> {
			Criteria criteria = Criteria.where(each.getKey()).is(each.getValue());
			query.addCriteria(criteria);
		});
		query.addCriteria(Criteria.where("del").is(DELETE.N));
		return query;
	}

	private Query valueToReturn(funding.common.Query querys) {
		Query query = new Query();
		querys.getKeysToReturn().stream().forEach(key -> {
			query.fields().include(key);
		});
		return query;
	}

}
