/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestParam;

import funding.common.Query.Builder;
import funding.common.Query.ORDER;
import funding.common.impl.QueryImpl;

public class SearchQueryImpl {
	private Builder<?> builder = QueryImpl.builder();

	public Builder<?> getBuilder() {
		return this.builder;
	}

	public SearchQueryImpl(@RequestParam("filter") String filter, @RequestParam("order") String order,
			@RequestParam("sort") String sort, @RequestParam("start") String start, @RequestParam("count") String count,
			@RequestParam("fields") String fields) {
		this.builder.sortBy(SearchQueryImpl.isNotBlank(sort) ? sort : "_id");
		if (fields != null) {
			List<String> f = new ArrayList<String>();
			String[] allFields = fields.split(",");
			for (String field : allFields) {
				f.add(field.trim());
			}
			this.builder.valuesToReturn(f);
		}
		if (filter != null) {
			Map<String, Object> filterQuery = this.filterQueryFormation(filter);
			this.builder.addFilter(filterQuery);
		}
		if (order != null) {
			if (order.equals("des")) {
				this.builder.sortOrder(ORDER.DEC);
			} else {
				this.builder.sortOrder(ORDER.ASC);
			}
		} else {
			this.builder.sortOrder(ORDER.ASC);
		}

		if (start != null) {
			this.builder.start(Integer.parseInt(start.trim()));
		} else {
			this.builder.start(QueryImpl.DEFAULTSTART);
		}
		if (count != null) {
			this.builder.count(Integer.parseInt(count.trim()));
		} else {
			this.builder.count(QueryImpl.DEFAULTCOUNT);
		}

	}

	private Map<String, Object> filterQueryFormation(String filter) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		String[] filterKey = filter.split(",");
		for (String eachKey : filterKey) {
			if (eachKey.matches(".*\\s(eqN).*")) {
				returnMap.put(eachKey.trim().split("eqN")[0].trim(),
						Integer.parseInt(eachKey.trim().split("eqN")[1].trim()));
			} else {
				String key = eachKey.trim().split("eq")[0].trim();
				String value = eachKey.trim().split("eq")[1].trim();
				returnMap.put(key, value);
			}
		}
		return returnMap;
	}

	private static boolean isNotBlank(CharSequence cs) {
		if (cs != null) {
			return true;
		} else {
			return false;
		}
	}
}
