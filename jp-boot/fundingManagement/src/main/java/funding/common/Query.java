/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.common;

import java.util.Collection;
import java.util.Map;

public interface Query {

	public final int DEFAULTCOUNT = 200;
	public final int DEFAULTSTART = 0;

	public Collection<String> getKeysToReturn();

	public int getStart();

	public int getCount();

	public String getSortBy();

	public ORDER getSortOrder();

	public Map<String, Object> getFilters();

	public enum ORDER {
		ASC, DEC
	}

	interface Builder<J extends Builder<J>> {
		J valuesToReturn(String key);

		J valuesToReturn(String key1, String key2);

		J valuesToReturn(String key1, String key2, String... keyn);

		J valuesToReturn(Collection<String> keys);

		J start(int startIndex);

		J count(int numberOfDocuments);

		J sortBy(String sortby);

		J sortOrder(ORDER order);

		J addFilter(Map<String, Object> map);

		J addFilter(String key, Object value);

		Query build();
	}

}
