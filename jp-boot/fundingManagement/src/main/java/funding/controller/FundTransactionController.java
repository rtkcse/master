/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.controller;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import funding.common.SearchQueryImpl;
import funding.custom.annotaions.BeanParam;
import funding.models.FundTransaction;
import funding.models.FundTransaction.FundTransactionType;
import funding.service.FundTransactionService;

@RestController
@RequestMapping(value = "/fund/transaction")
public class FundTransactionController {

	@Autowired
	FundTransactionService fundTransactionService;

	@PostMapping
	public FundTransaction createCategory(@Valid @RequestBody FundTransaction transaction) {
		transaction.setTransactionDate(new Date().getTime());
		if (transaction.getTransactionType().equals(FundTransactionType.DR)) {
			Double amount = (transaction.getAmount() * -1);
			transaction.setAmount(amount);
		}
		return fundTransactionService.create(transaction);
	}

	@GetMapping
	public List<FundTransaction> getFundTransaction(@BeanParam SearchQueryImpl searchQuery) {
		return fundTransactionService.get(searchQuery.getBuilder().build());
	}

	@GetMapping("/{id}")
	public FundTransaction getCategoryById(@PathVariable("id") String id, @BeanParam SearchQueryImpl searchQuery) {
		return fundTransactionService.getById(id, searchQuery.getBuilder().build());
	}

	@PutMapping
	public FundTransaction updateCategory(@PathVariable("id") String id,
			@Valid @ModelAttribute FundTransaction transaction) {
		return fundTransactionService.update(id, transaction);
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable("id") String id) {
		this.fundTransactionService.deleteById(id);
	}

	@GetMapping("/balance")
	public Double getBalanceAmount(@BeanParam SearchQueryImpl searchQuery) {
		List<FundTransaction> allTransactions = this.fundTransactionService.get(searchQuery.getBuilder().build());
		Double values = allTransactions.stream().map(each -> each.getAmount()).reduce((double) 0, (a, b) -> a + b);
		return values;

	}

}