/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import funding.common.SearchQueryImpl;
import funding.common.Query.ORDER;
import funding.common.impl.QueryImpl;
import funding.custom.annotaions.BeanParam;
import funding.models.ExpensesCategory;
import funding.service.ExpensesCategoryService;
@RestController
@RequestMapping(value = "/expenses/category")
public class ExpensesCategoryController {

	@Autowired
	ExpensesCategoryService categoryService;

	@PostMapping
	public ExpensesCategory createCategory(@Valid @RequestBody ExpensesCategory category) {
		return categoryService.create(category);
	}
	@GetMapping
	public List<ExpensesCategory> getCategories(@BeanParam SearchQueryImpl searchQuery) {
		return categoryService.get(searchQuery.getBuilder().build());
	}

	@GetMapping("/{id}")
	public ExpensesCategory getCategoryById(@PathVariable("id") String id, @BeanParam SearchQueryImpl searchQuery) {
		return categoryService.getById(id, searchQuery.getBuilder().build());
	}
	
	@PutMapping("/{id}")
	public ExpensesCategory updateCategory(@PathVariable("id") String id,
			@Valid @RequestBody ExpensesCategory category) {
		return categoryService.update(id, category);
	}
	
	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable("id") String id) {
		this.categoryService.deleteById(id);
	}

}