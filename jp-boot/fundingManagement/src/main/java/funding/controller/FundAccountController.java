/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import funding.common.SearchQueryImpl;
import funding.common.Query.ORDER;
import funding.common.impl.QueryImpl;
import funding.custom.annotaions.BeanParam;
import funding.models.ExpensesCategory;
import funding.models.FundAccount;
import funding.models.FundTransaction;
import funding.service.ExpensesCategoryService;
import funding.service.FundAccountService;
import funding.service.FundTransactionService;

@RestController
@RequestMapping(value = "/fund/account")
public class FundAccountController {

	@Autowired
	FundAccountService accountService;

	@PostMapping
	public FundAccount createCategory(@Valid @RequestBody FundAccount category) {
		return accountService.create(category);
	}

	@GetMapping
	public List<FundAccount> getCategories(@BeanParam SearchQueryImpl searchQuery) {
		return accountService.get(QueryImpl.builder().build());
	}

	@GetMapping("/{id}")
	public FundAccount getCategoryById(@PathVariable("id") String id, @BeanParam SearchQueryImpl searchQuery) {
		return accountService.getById(id, searchQuery.getBuilder().build());
	}

	@PutMapping("/{id}")
	public FundAccount updateCategory(@PathVariable("id") String id, @Valid @RequestBody FundAccount category) {
		System.out.println(id);
		return accountService.update(id, category);
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable("id") String id) {
		this.accountService.deleteById(id);
	}

}