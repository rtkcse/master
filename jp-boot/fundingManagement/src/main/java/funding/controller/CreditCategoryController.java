/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import funding.common.SearchQueryImpl;
import funding.custom.annotaions.BeanParam;
import funding.models.CreditCategory;
import funding.service.CreditCategoryService;

@RestController
@RequestMapping(value = "/credit/category")
public class CreditCategoryController {

	@Autowired
	CreditCategoryService creditCategoryService;

	@PostMapping
	public CreditCategory createCategory(@Valid @RequestBody CreditCategory category) {
		return creditCategoryService.create(category);
	}

	@GetMapping
	public List<CreditCategory> getCategories(@BeanParam SearchQueryImpl searchQuery) {
		return creditCategoryService.get(searchQuery.getBuilder().build());
	}

	@GetMapping("/{id}")
	public CreditCategory getCategoryById(@PathVariable("id") String id, @BeanParam SearchQueryImpl searchQuery) {
		return creditCategoryService.getById(id, searchQuery.getBuilder().build());
	}

	@PutMapping("/{id}")
	public CreditCategory updateCategory(@PathVariable("id") String id, @Valid @RequestBody CreditCategory category) {
		return creditCategoryService.update(id, category);
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable("id") String id) {
		this.creditCategoryService.deleteById(id);
	}

}