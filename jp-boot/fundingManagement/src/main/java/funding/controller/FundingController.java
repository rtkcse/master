/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import funding.common.SearchQueryImpl;
import funding.custom.annotaions.BeanParam;
import funding.models.ExpensesCategory;
import funding.models.Funding;
import funding.service.ExpensesCategoryService;
import funding.service.FundingService;
@RestController
@RequestMapping(value = "/expenses/fund")
public class FundingController {

	@Autowired
	FundingService fundingService;

	@PostMapping
	public Funding addFund(@Valid @ModelAttribute Funding fund) {
		return fundingService.addFund(fund);
	}
	@GetMapping
	public List<Funding> getFunds(@BeanParam SearchQueryImpl searchQuery) {
		return fundingService.get(searchQuery.getBuilder().build());
	}

	@GetMapping("/{id}")
	public Funding getFundById(@PathVariable("id") String id, @BeanParam SearchQueryImpl searchQuery) {
		return fundingService.getById(id, searchQuery.getBuilder().build());
	}
	
	@PutMapping
	public Funding updateFund(@PathVariable("id") String id,
			@Valid @ModelAttribute Funding fund) {
		return fundingService.update(id, fund);
	}
	
	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable("id") String id) {
		this.fundingService.deleteById(id);
	}

}