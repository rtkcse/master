/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import funding.common.SearchQueryImpl;
import funding.custom.annotaions.BeanParam;
import funding.models.FundingReport;
import funding.service.FundingReportService;

@RestController
@RequestMapping(value = "/fund/report")
public class FundingReportController {

	@Autowired
	FundingReportService fundingReportService;

	@GetMapping("/{fundAccountId}/{from}/{to}")
	public FundingReport getCategories(@BeanParam SearchQueryImpl searchQuery,
			@PathVariable("fundAccountId") String fundAccountId, @PathVariable("from") long fromDate,
			@PathVariable("to") long toDate) {
		return fundingReportService.getChartReport(fundAccountId, fromDate, toDate);

	}
}