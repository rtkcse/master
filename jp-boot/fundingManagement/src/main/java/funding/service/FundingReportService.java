/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.service;

import funding.models.FundingReport;

public interface FundingReportService {

	FundingReport getChartReport(String fundAccountId, long fromDate, long toDate);

}