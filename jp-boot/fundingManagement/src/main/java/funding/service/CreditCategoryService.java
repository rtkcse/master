/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.service;

import funding.models.CreditCategory;

public interface CreditCategoryService extends CommonService<CreditCategory> {

}