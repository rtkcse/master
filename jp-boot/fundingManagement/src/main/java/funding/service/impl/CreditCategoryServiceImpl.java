/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import funding.common.Query;
import funding.dao.CommonRepository;
import funding.models.CreditCategory;
import funding.models.ExpensesCategory;
import funding.service.CreditCategoryService;

@Service
public class CreditCategoryServiceImpl extends AbstractServiceImpl<CreditCategory> implements CreditCategoryService {

	@Autowired
	private CommonRepository<CreditCategory> commonRepository;

	@Override
	public List<CreditCategory> get(Query query) {
		return this.commonRepository.get(query, CreditCategory.class);
	}

	@Override
	public CreditCategory getById(String id, Query query) {
		return this.commonRepository.getById(id, query, CreditCategory.class);
	}

	@Override
	public void deleteById(String id) {
		this.commonRepository.delete(id, CreditCategory.class);
	}

}