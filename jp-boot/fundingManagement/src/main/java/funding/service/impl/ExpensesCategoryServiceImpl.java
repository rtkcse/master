/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import funding.common.Query;
import funding.dao.CommonRepository;
import funding.models.ExpensesCategory;
import funding.service.ExpensesCategoryService;

@Service
public class ExpensesCategoryServiceImpl extends AbstractServiceImpl<ExpensesCategory>
		implements ExpensesCategoryService {

	@Autowired
	private CommonRepository<ExpensesCategory> commonRepository;

	@Override
	public List<ExpensesCategory> get(Query query) {
		return this.commonRepository.get(query, ExpensesCategory.class);
	}

	@Override
	public ExpensesCategory getById(String id, Query query) {
		return this.commonRepository.getById(id, query, ExpensesCategory.class);
	}

	@Override
	public void deleteById(String id) {
		System.out.println(id);
		this.commonRepository.delete(id, ExpensesCategory.class);
	}

}