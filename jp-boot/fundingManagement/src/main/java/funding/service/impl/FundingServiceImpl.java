/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import funding.common.Query;
import funding.common.impl.QueryImpl;
import funding.dao.CommonRepository;
import funding.models.Funding;
import funding.models.FundingDashBoard;
import funding.service.FundingService;

@Service
public class FundingServiceImpl extends AbstractServiceImpl<Funding>
		implements FundingService {

	@Autowired
	private CommonRepository<Funding> commonRepository;
	
	@Autowired
	private CommonRepository<FundingDashBoard> dashBoardRepo;

	@Override
	public List<Funding> get(Query query) {
		return this.commonRepository.get(query, Funding.class);
	}

	@Override
	public Funding getById(String id, Query query) {
		return this.commonRepository.getById(id, query, Funding.class);
	}

	@Override
	public void deleteById(String id) {
		this.commonRepository.delete(id, Funding.class);
	}

	@Override
	public Funding addFund(Funding fund) {
		List<FundingDashBoard> dashBoard = this.dashBoardRepo.get(QueryImpl.builder().build(), FundingDashBoard.class);
		if (dashBoard != null && !dashBoard.isEmpty()) {
			fund.setLedgerBalance(dashBoard.get(0).getClearBalance());
			fund.setClearBalance(dashBoard.get(0).getClearBalance() + fund.getCredit());
			Funding funding = this.create(fund);
			dashBoard.get(0).setClearBalance(funding.getClearBalance());
			FundingDashBoard board = dashBoard.get(0);
			this.dashBoardRepo.update(dashBoard.get(0).getId(), board);
			return funding;
		} else {
			FundingDashBoard document = new FundingDashBoard();
			document.setClearBalance((double) 0);
			FundingDashBoard board = this.dashBoardRepo.create(document);
			fund.setLedgerBalance(board.getClearBalance());
			fund.setClearBalance(board.getClearBalance() + fund.getCredit());
			Funding funding = this.create(fund);
			board.setClearBalance(funding.getClearBalance());
			this.dashBoardRepo.update(board.getId(), board);
			return funding;
		}

	}

}