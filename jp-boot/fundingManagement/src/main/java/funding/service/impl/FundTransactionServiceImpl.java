/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import funding.common.Query;
import funding.dao.CommonRepository;
import funding.models.FundTransaction;
import funding.service.FundTransactionService;

@Service
public class FundTransactionServiceImpl extends AbstractServiceImpl<FundTransaction> implements FundTransactionService {

	@Autowired
	private CommonRepository<FundTransaction> commonRepository;

	@Override
	public List<FundTransaction> get(Query query) {
		List<FundTransaction> fundTransactions = new ArrayList<FundTransaction>();
		fundTransactions = this.commonRepository.get(query, FundTransaction.class);
		return fundTransactions;
	}

	@Override
	public FundTransaction getById(String id, Query query) {
		return this.commonRepository.getById(id, query, FundTransaction.class);
	}

	@Override
	public void deleteById(String id) {
		this.commonRepository.delete(id, FundTransaction.class);
	}

}