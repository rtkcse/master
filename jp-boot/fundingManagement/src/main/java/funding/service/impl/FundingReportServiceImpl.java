/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import funding.common.impl.QueryImpl;
import funding.models.FundTransaction;
import funding.models.FundingReport;
import funding.models.FundTransaction.FundTransactionType;
import funding.models.FundingReport.Credit;
import funding.models.FundingReport.Debit;
import funding.service.CreditCategoryService;
import funding.service.ExpensesCategoryService;
import funding.service.FundAccountService;
import funding.service.FundTransactionService;
import funding.service.FundingReportService;

@Service
public class FundingReportServiceImpl implements FundingReportService {

	@Autowired
	private FundTransactionService fundTransactionService;
	@Autowired
	private CreditCategoryService creditCategoryService;
	@Autowired
	private ExpensesCategoryService expensesCategoryService;
	@Autowired
	private FundAccountService fundAccountService;

	@Override
	public FundingReport getChartReport(String fundAccountId, long fromDate, long toDate) {
		FundingReport fundingReport = new FundingReport();
		List<Credit> credits = new ArrayList<FundingReport.Credit>();
		List<Debit> debits = new ArrayList<FundingReport.Debit>();
		Set<String> allCreditCategotyIds = new HashSet<String>();
		Set<String> allDebitCategotyIds = new HashSet<String>();
		String accountName = fundAccountService.getById(fundAccountId, QueryImpl.builder().build()).getAccountName();
		fundingReport.setReportName(accountName);
		fundingReport.setfrom(String.valueOf(fromDate));
		fundingReport.setTo(String.valueOf(toDate));
		List<FundTransaction> allFundTransactions = this.fundTransactionService.get(QueryImpl.builder().build());
		List<FundTransaction> allDebitTransactions = allFundTransactions.stream()
				.filter(eachTransaction -> eachTransaction.getFundAccountId().equals(fundAccountId))
				.filter(eachTransaction -> (eachTransaction.getTransactionDate() >= fromDate
						&& eachTransaction.getTransactionDate() <= toDate))
				.filter(eachTransaction -> eachTransaction.getTransactionType().equals(FundTransactionType.DR))
				.collect(Collectors.toList());

		List<FundTransaction> allCreditTransactions = allFundTransactions.stream()
				.filter(eachTransaction -> eachTransaction.getFundAccountId().equals(fundAccountId))
				.filter(eachTransaction -> (eachTransaction.getTransactionDate() >= fromDate
						&& eachTransaction.getTransactionDate() <= toDate))
				.filter(eachTransaction -> eachTransaction.getTransactionType().equals(FundTransactionType.CR))
				.collect(Collectors.toList());

		allCreditCategotyIds = allCreditTransactions.stream().map(each -> each.getCategoryID())
				.collect(Collectors.toSet());
		double totalCredit = allCreditTransactions.stream().map(each -> each.getAmount()).reduce((double) 0,
				(a, b) -> a + b);
		double totalDebit = allDebitTransactions.stream().map(each -> each.getAmount()).reduce((double) 0,
				(a, b) -> a + b);
		allDebitCategotyIds = allDebitTransactions.stream().map(each -> each.getCategoryID())
				.collect(Collectors.toSet());
		allDebitCategotyIds.forEach(categotyId -> {
			Debit debit = new Debit();
			double totalDebitAmount = 0;
			totalDebitAmount = allDebitTransactions.stream().filter(each -> each.getCategoryID().equals(categotyId))
					.map(each -> each.getAmount()).reduce((double) 0, (a, b) -> a + b);
			debit.setCategoryName(expensesCategoryService.getById(categotyId, QueryImpl.builder().build()).getName());
			debit.setAmount(Math.abs(totalDebitAmount));
			double per = (totalDebitAmount * 100) / (totalDebit);
			debit.setPercentage(per);
			debits.add(debit);
		});
		fundingReport.setDebit(debits);
		fundingReport.setTotalAmountDebited(Math.abs(totalDebit));

		allCreditCategotyIds.forEach(categotyId -> {
			Credit credit = new Credit();
			double totalCreditAmount = 0;
			totalCreditAmount = allCreditTransactions.stream().filter(each -> each.getCategoryID().equals(categotyId))
					.map(each -> each.getAmount()).reduce((double) 0, (a, b) -> a + b);
			credit.setCategoryName(creditCategoryService.getById(categotyId, QueryImpl.builder().build()).getName());
			credit.setAmount(totalCreditAmount);
			double percentage = (totalCreditAmount * 100) / (totalCredit);
			credit.setPercentage(percentage);
			credits.add(credit);
		});

		fundingReport.setCredit(credits);
		fundingReport.setTotalAmountCredited(totalCredit);
		return fundingReport;

	}

}