/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import funding.common.Query;
import funding.dao.CommonRepository;
import funding.models.ExpensesCategory;
import funding.models.FundAccount;
import funding.service.ExpensesCategoryService;
import funding.service.FundAccountService;

@Service
public class FundAccountServiceImpl extends AbstractServiceImpl<FundAccount>
		implements FundAccountService {

	@Autowired
	private CommonRepository<FundAccount> commonRepository;

	@Override
	public List<FundAccount> get(Query query) {
		return this.commonRepository.get(query, FundAccount.class);
	}

	@Override
	public FundAccount getById(String id, Query query) {
		return this.commonRepository.getById(id, query, FundAccount.class);
	}

	@Override
	public void deleteById(String id) {
		this.commonRepository.delete(id, FundAccount.class);
	}

}