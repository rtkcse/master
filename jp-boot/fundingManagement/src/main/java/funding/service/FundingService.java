/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package funding.service;

import funding.models.Funding;

public interface FundingService extends CommonService<Funding>{
	public Funding addFund(Funding fund);

	
}