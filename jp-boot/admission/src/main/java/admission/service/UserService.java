/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package admission.service;

import admission.models.User;

public interface UserService extends CommonService<User> {

	public User login(User user);

}