/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package admission.service;

import admission.models.Application;

public interface ApplicationService extends CommonService<Application> {

}