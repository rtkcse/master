/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package admission.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import admission.common.Query;
import admission.dao.CommonRepository;
import admission.models.Application;
import admission.service.ApplicationService;

@Service
public class ApplicationServiceImpl extends AbstractServiceImpl<Application> implements ApplicationService {

	@Autowired
	private CommonRepository<Application> commonRepository;

	@Override
	public List<Application> get(Query query) {
		return this.commonRepository.get(query, Application.class);
	}

	@Override
	public Application getById(String id, Query query) {
		return this.commonRepository.getById(id, query, Application.class);
	}

	@Override
	public void deleteById(String id) {
		this.commonRepository.delete(id, Application.class);
	}

//	@Override
//	public Application create(Application application) {
//		return this.commonRepository.create(application);
//	}

}