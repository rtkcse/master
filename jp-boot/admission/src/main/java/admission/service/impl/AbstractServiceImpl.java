/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package admission.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import admission.dao.CommonRepository;
import admission.models.Document;
import admission.service.CommonService;

public abstract class AbstractServiceImpl<T extends Document> implements CommonService<T> {
	@Autowired
	protected CommonRepository<T> common;

	@Override
	public T create(T newDocument) {
		return this.common.create(newDocument);
	}

	@Override
	public T update(String id, T newDocument) {
		return this.common.update(id, newDocument);
	}

}
