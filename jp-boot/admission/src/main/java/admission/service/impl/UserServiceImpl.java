/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package admission.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import admission.common.Query;
import admission.common.impl.QueryImpl;
import admission.dao.CommonRepository;
import admission.models.User;
import admission.service.UserService;

@Service
public class UserServiceImpl extends AbstractServiceImpl<User> implements UserService {

	@Autowired
	private CommonRepository<User> commonRepository;

	@Override
	public List<User> get(Query query) {
		List<User> users = this.commonRepository.get(query, User.class);
		users.stream().forEachOrdered(user -> this.ignoreCredentials(user));
		return users;
	}

	@Override
	public User getById(String id, Query query) {
		User user = this.commonRepository.getById(id, query, User.class);
		this.ignoreCredentials(user);
		return user;
	}

	@Override
	public void deleteById(String id) {
		this.commonRepository.delete(id, User.class);
	}

	@Override
	public User create(User user) {
		if (checkUserEmailExits(user)) {
			user.setStatus("exits");
			this.ignoreCredentials(user);
			return user;
		}
		user.getEmail().toLowerCase();
		User userData = this.commonRepository.create(user);
		this.ignoreCredentials(userData);
		return userData;
	}

	@Override
	public User update(String id, User newDocument) {
		System.out.println(id);
		User user = this.commonRepository.getById(id, QueryImpl.builder().build(), User.class);
		this.bindUserInfo(user, newDocument);
		return this.commonRepository.update(id, newDocument);
	}

	private void bindUserInfo(User user, User newDocument) {
		newDocument.setPassword(user.getPassword());
		newDocument.setCreatedOn(user.getCreatedOn());
	}

	@Override
	public User login(User user) {
		List<User> userData = this.get(QueryImpl.builder().addFilter("email", user.getEmail().toLowerCase())
				.addFilter("password", user.getPassword()).build());
		if (userData.isEmpty()) {
			user.setStatus("Incorrect email or Password.");
			return user;
		}
		User currentUser = userData.get(0);
		currentUser.setStatus("success");
		this.ignoreCredentials(currentUser);
		return currentUser;
	}

	private void ignoreCredentials(User user) {
		user.setPassword(null);
	}

	private boolean checkUserEmailExits(User user) {
		List<User> users = this.get(QueryImpl.builder().addFilter("email", user.getEmail()).build());
		if (users.isEmpty()) {
			return false;
		}

		return true;

	}

	private boolean isValid(String value) {
		return value.isEmpty();
	}

}