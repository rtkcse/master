/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package admission.common.exception;

public class AlreadyExists extends Exception {

	public AlreadyExists(String message) {
		super(message);
	}
}
