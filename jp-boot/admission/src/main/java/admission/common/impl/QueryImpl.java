/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package admission.common.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import admission.common.Query;

public class QueryImpl implements Query {
	private int startIndex;
	private int count;
	private String sortBy;
	private ORDER order;
	private Map<String, Object> filters;
	private Collection<String> returnKeys;

	private QueryImpl(int page, int count, String sortBy, ORDER order, Map<String, Object> filters,
			Collection<String> returnKeys) {
		this.startIndex = page;
		this.count = count;
		this.sortBy = sortBy;
		this.order = order;
		this.filters = filters;
		this.returnKeys = returnKeys;
	}

	@Override
	public Collection<String> getKeysToReturn() {
		return this.returnKeys;
	}

	@Override
	public int getStart() {
		return this.startIndex;
	}

	@Override
	public int getCount() {
		return this.count;
	}

	@Override
	public String getSortBy() {
		return this.sortBy;
	}

	@Override
	public ORDER getSortOrder() {
		return this.order;
	}

	@Override
	public Map<String, Object> getFilters() {
		return this.filters;
	}

	public static Builder builder() {
		return new QueryBuilder();
	}

	private static class QueryBuilder implements Builder<QueryBuilder> {
		private Collection<String> returnValues = new HashSet<String>();
		private int startIndex;
		private int count;
		private String sortBy;
		private ORDER order;
		private Map<String, Object> filters = new HashMap<String, Object>();

		@Override
		public QueryBuilder valuesToReturn(String key) {
			this.returnValues.add(key);
			return self();
		}

		@Override
		public QueryBuilder valuesToReturn(String key1, String key2) {
			this.returnValues.add(key1);
			this.returnValues.add(key2);
			return self();
		}

		@Override
		public QueryBuilder valuesToReturn(String key1, String key2, String... keyn) {
			this.returnValues.add(key1);
			this.returnValues.add(key2);
			for (String key : keyn) {
				this.returnValues.add(key);
			}
			return self();
		}

		@Override
		public QueryBuilder valuesToReturn(Collection<String> returnValues) {
			this.returnValues.addAll(returnValues);
			return self();
		}

		@Override
		public QueryBuilder start(int startIndex) {
			this.startIndex = (startIndex < 1) ? DEFAULTSTART : startIndex;
			return self();
		}

		@Override
		public QueryBuilder count(int numberOfDocuments) {
			this.count = (numberOfDocuments < 1) ? DEFAULTCOUNT : numberOfDocuments;
			return self();
		}

		@Override
		public QueryBuilder sortBy(String sortBy) {
			this.sortBy = sortBy;
			return self();
		}

		@Override
		public QueryBuilder sortOrder(ORDER order) {
			this.order = (order == null) ? ORDER.ASC : order;
			return self();
		}

		@Override
		public QueryBuilder addFilter(Map<String, Object> filter) {
			if (filter != null) {
				this.filters.putAll(filter);
			}
			return this;
		}

		private QueryBuilder self() {
			return this;
		}

		@Override
		public Query build() {
			return new QueryImpl(this.startIndex, this.count, this.sortBy, this.order, this.filters, this.returnValues);
		}

		@Override
		public QueryBuilder addFilter(String key, Object value) {
			Map<String, Object> filter = new HashMap<String, Object>();
			filter.put(key, value);
			this.filters.putAll(filter);
			return this;
		}
	}
}
