/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package admission.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import admission.common.SearchQueryImpl;
import admission.custom.annotaions.BeanParam;
import admission.models.Application;
import admission.service.ApplicationService;

@RestController
@RequestMapping(value = "/application")
public class ApplicationController {

	@Autowired
	ApplicationService applicationService;

	@PostMapping
	public Application addFund(@Valid @RequestBody Application application) {
		return applicationService.create(application);
	}

	@GetMapping
	public List<Application> getApplications(@BeanParam SearchQueryImpl searchQuery) {
		return applicationService.get(searchQuery.getBuilder().build());
	}

	@GetMapping("/{id}")
	public Application getApplicationById(@PathVariable("id") String id, @BeanParam SearchQueryImpl searchQuery) {
		return applicationService.getById(id, searchQuery.getBuilder().build());
	}

	@PutMapping("/{id}")
	public Application updateApplication(@PathVariable("id") String id, @Valid @RequestBody Application application) {
		return applicationService.update(id, application);
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable("id") String id) {
		this.applicationService.deleteById(id);
	}
}