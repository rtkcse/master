/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package admission.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import admission.common.SearchQueryImpl;
import admission.custom.annotaions.BeanParam;
import admission.models.User;
import admission.service.UserService;

@RestController
@RequestMapping(value = "/user")
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping
	public User createUser(@Valid @RequestBody User user) {
		return userService.create(user);
	}

	@PostMapping("/login")
	public User login(@Valid @RequestBody User user) {
		return userService.login(user);
	}

	@GetMapping
	public List<User> getUsers(@BeanParam SearchQueryImpl searchQuery) {
		return userService.get(searchQuery.getBuilder().build());
	}

	@GetMapping("/{id}")
	public User getUserById(@PathVariable("id") String id, @BeanParam SearchQueryImpl searchQuery) {
		return userService.getById(id, searchQuery.getBuilder().build());
	}

	@PutMapping("/{id}")
	public User updateUser(@PathVariable("id") String id, @Valid @RequestBody User user) {
		return userService.update(id, user);
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable("id") String id) {
		this.userService.deleteById(id);
	}
}