/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package admission.models;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Parents extends Document<Parents> {
	private static final long serialVersionUID = 1L;

	public Parents() {
	}

	private String fathersTitle;
	private String fathersName;
	private String fathersOccupation;
	private String fathersAnnualIncome;
	private String fathersMobile;
	private String fathersAlterMobile;
	private String fathersEmail;
	private String mothersTitle;
	private String mothersName;
	private String mothersOccupation;
	private String mothersAnnualIncome;
	private String mothersMobile;
	private String mohtersAlterMobile;
	private String mothersEmail;
	private boolean guardiansDetails;
	private String guardiansTitle;
	private String guardiansName;
	private String guardiansOccupation;
	private String guardiansMobile;
	private String guardiansEmail;

	public String getFathersTitle() {
		return fathersTitle;
	}

	public void setFathersTitle(String fathersTitle) {
		this.fathersTitle = fathersTitle;
	}

	public String getFathersName() {
		return fathersName;
	}

	public void setFathersName(String fathersName) {
		this.fathersName = fathersName;
	}

	public String getFathersOccupation() {
		return fathersOccupation;
	}

	public void setFathersOccupation(String fathersOccupation) {
		this.fathersOccupation = fathersOccupation;
	}

	public String getFathersAnnualIncome() {
		return fathersAnnualIncome;
	}

	public void setFathersAnnualIncome(String fathersAnnualIncome) {
		this.fathersAnnualIncome = fathersAnnualIncome;
	}

	public String getFathersMobile() {
		return fathersMobile;
	}

	public void setFathersMobile(String fathersMobile) {
		this.fathersMobile = fathersMobile;
	}

	public String getFathersAlterMobile() {
		return fathersAlterMobile;
	}

	public void setFathersAlterMobile(String fathersAlterMobile) {
		this.fathersAlterMobile = fathersAlterMobile;
	}

	public String getFathersEmail() {
		return fathersEmail;
	}

	public void setFathersEmail(String fathersEmail) {
		this.fathersEmail = fathersEmail;
	}

	public String getMothersTitle() {
		return mothersTitle;
	}

	public void setMothersTitle(String mothersTitle) {
		this.mothersTitle = mothersTitle;
	}

	public String getMothersName() {
		return mothersName;
	}

	public void setMothersName(String mothersName) {
		this.mothersName = mothersName;
	}

	public String getMothersOccupation() {
		return mothersOccupation;
	}

	public void setMothersOccupation(String mothersOccupation) {
		this.mothersOccupation = mothersOccupation;
	}

	public String getMothersAnnualIncome() {
		return mothersAnnualIncome;
	}

	public void setMothersAnnualIncome(String mothersAnnualIncome) {
		this.mothersAnnualIncome = mothersAnnualIncome;
	}

	public String getMothersMobile() {
		return mothersMobile;
	}

	public void setMothersMobile(String mothersMobile) {
		this.mothersMobile = mothersMobile;
	}

	public String getMohtersAlterMobile() {
		return mohtersAlterMobile;
	}

	public void setMohtersAlterMobile(String mohtersAlterMobile) {
		this.mohtersAlterMobile = mohtersAlterMobile;
	}

	public String getMothersEmail() {
		return mothersEmail;
	}

	public void setMothersEmail(String mothersEmail) {
		this.mothersEmail = mothersEmail;
	}

	public boolean isGuardiansDetails() {
		return guardiansDetails;
	}

	public void setGuardiansDetails(boolean guardiansDetails) {
		this.guardiansDetails = guardiansDetails;
	}

	public String getGuardiansTitle() {
		return guardiansTitle;
	}

	public void setGuardiansTitle(String guardiansTitle) {
		this.guardiansTitle = guardiansTitle;
	}

	public String getGuardiansName() {
		return guardiansName;
	}

	public void setGuardiansName(String guardiansName) {
		this.guardiansName = guardiansName;
	}

	public String getGuardiansOccupation() {
		return guardiansOccupation;
	}

	public void setGuardiansOccupation(String guardiansOccupation) {
		this.guardiansOccupation = guardiansOccupation;
	}

	public String getGuardiansMobile() {
		return guardiansMobile;
	}

	public void setGuardiansMobile(String guardiansMobile) {
		this.guardiansMobile = guardiansMobile;
	}

	public String getGuardiansEmail() {
		return guardiansEmail;
	}

	public void setGuardiansEmail(String guardiansEmail) {
		this.guardiansEmail = guardiansEmail;
	}

}