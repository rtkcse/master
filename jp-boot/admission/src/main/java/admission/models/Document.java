/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package admission.models;

import java.io.Serializable;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@EntityScan
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(value = { "id" }, allowGetters = true)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, 
setterVisibility = JsonAutoDetect.Visibility.NONE)
public abstract class Document<T> implements Comparable<T>, Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	private String createdOn;
	private String modifiedOn;
	@JsonIgnore
	private DELETE del;

	public enum DELETE {
		Y, N
	};

	public DELETE getDel() {
		return del;
	}

	public void setDel(DELETE del) {
		this.del = del;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Document() {

	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Override
	public int compareTo(T o) {
		Document<?> document = (Document<?>) o;
		if (this.getCreatedOn() == null || document.getCreatedOn() == null) {
			return 0;
		}
		return this.getCreatedOn().compareTo(document.getCreatedOn());
	}

}
