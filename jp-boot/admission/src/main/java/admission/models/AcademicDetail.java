/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package admission.models;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class AcademicDetail extends Document<AcademicDetail> {
	private static final long serialVersionUID = 1L;

	public AcademicDetail() {
	}

	private String instituteName;
	private String board;
	private String markingScheme;
	private String markObtained;
	private String yearOfPassing;
	private String registrationNo;
	private String university;
	private String modeOfStudy;

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getMarkingScheme() {
		return markingScheme;
	}

	public void setMarkingScheme(String markingScheme) {
		this.markingScheme = markingScheme;
	}

	public String getMarkObtained() {
		return markObtained;
	}

	public void setMarkObtained(String markObtained) {
		this.markObtained = markObtained;
	}

	public String getYearOfPassing() {
		return yearOfPassing;
	}

	public void setYearOfPassing(String yearOfPassing) {
		this.yearOfPassing = yearOfPassing;
	}

	public String getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public String getUniversity() {
		return university;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public String getModeOfStudy() {
		return modeOfStudy;
	}

	public void setModeOfStudy(String modeOfStudy) {
		this.modeOfStudy = modeOfStudy;
	}

}