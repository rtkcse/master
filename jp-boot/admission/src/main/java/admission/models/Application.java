/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package admission.models;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;

import admission.custom.annotaions.DocumentName;

@DocumentName(collection = "application")
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Application extends Document<Application> {
	private static final long serialVersionUID = 1L;

	public Application() {
	}

	@NotBlank
	private String name;

	@NotNull(message = "Application Fees Shoud not be empty")
	private Double fees;
	private List<String> rule;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getFees() {
		return fees;
	}

	public void setFees(Double fees) {
		this.fees = fees;
	}

	public List<String> getRule() {
		return rule;
	}

	public void setRule(List<String> rule) {
		this.rule = rule;
	}

}