/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package admission.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import admission.custom.annotaions.DocumentName;

@DocumentName(collection = "user")
public class User extends Document<User> {
	private static final long serialVersionUID = 1L;

	public User() {
	}

	@JsonInclude(Include.NON_NULL)
	private String title;
	private String firstName;
	private String middleName;
	private String lastName;
	private String mobile;
	private String email;
	private String alternateEmail;
	private String dateOfBirth;
	private String password;
	private String gender;
	private String bloodGroup;
	private String community;
	private String religion;
	private String motherTongue;
	private String placeOfBirth;
	private String sourceOfAdmission;
	private String differentlyAbled;
	private String studiedFromIndia;
	private String nationality;
	private String status;

	// parents Details
	private Parents parentsDetails;

	// AcademicDetails
	private AcademicDetails academicDetails;

	// Communication Details
	private CommunicationDetails communicationDetails;

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {
		User user = new User();

		public Builder title(String title) {
			this.user.setTitle(title);
			return this;
		}

		public Builder firstName(String firstName) {
			this.user.setFirstName(firstName);
			return this;
		}

		public Builder lastName(String lastName) {
			this.user.setLastName(lastName);
			return this;
		}

		public Builder middleName(String middleName) {
			this.user.setMiddleName(middleName);
			return this;
		}

		public Builder mobile(String mobile) {
			this.user.setMobile(mobile);
			return this;
		}

		public Builder email(String email) {
			this.user.setEmail(email);
			return this;
		}

		public Builder alternateEmail(String alternateEmail) {
			this.user.setAlternateEmail(alternateEmail);
			return this;
		}

		public Builder dateOfBirth(String dateOfBirth) {
			this.user.setDateOfBirth(dateOfBirth);
			return this;
		}

		public Builder password(String password) {
			this.user.setPassword(password);
			return this;
		}

		public Builder gender(String gender) {
			this.user.setGender(gender);
			return this;
		}

		public Builder bloodGroup(String bloodGroup) {
			this.user.setBloodGroup(bloodGroup);
			return this;
		}

		public Builder community(String community) {
			this.user.setCommunity(community);
			return this;
		}

		public Builder religion(String religion) {
			this.user.setReligion(religion);
			return this;
		}

		public Builder motherTongue(String motherTongue) {
			this.user.setMotherTongue(motherTongue);
			return this;
		}

		public Builder placeOfBirth(String placeOfBirth) {
			this.user.setPlaceOfBirth(placeOfBirth);
			return this;
		}

		public Builder sourceOfAdmission(String sourceOfAdmission) {
			this.user.setSourceOfAdmission(sourceOfAdmission);
			return this;
		}

		public Builder differentlyAbled(String differentlyAbled) {
			this.user.setDifferentlyAbled(differentlyAbled);
			return this;
		}

		public Builder status(String status) {
			this.user.setStatus(status);
			return this;
		}

		public Builder academicDetails(AcademicDetails academicDetails) {
			this.user.setAcademicDetails(academicDetails);
			return this;
		}

		public Builder parentsDetails(Parents parentsDetails) {
			this.user.setParentsDetails(parentsDetails);
			return this;
		}

		public Builder communicationDetails(CommunicationDetails communicationDetails) {
			this.user.setCommunicationDetails(communicationDetails);
			return this;
		}

		public User build() {
			return this.user;
		}
	}

	public String getStudiedFromIndia() {
		return studiedFromIndia;
	}

	public void setStudiedFromIndia(String studiedFromIndia) {
		this.studiedFromIndia = studiedFromIndia;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Parents getParentsDetails() {
		return parentsDetails;
	}

	public void setParentsDetails(Parents parentsDetails) {
		this.parentsDetails = parentsDetails;
	}

	public AcademicDetails getAcademicDetails() {
		return academicDetails;
	}

	public void setAcademicDetails(AcademicDetails academicDetails) {
		this.academicDetails = academicDetails;
	}

	public CommunicationDetails getCommunicationDetails() {
		return communicationDetails;
	}

	public void setCommunicationDetails(CommunicationDetails communicationDetails) {
		this.communicationDetails = communicationDetails;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAlternateEmail() {
		return alternateEmail;
	}

	public void setAlternateEmail(String alternateEmail) {
		this.alternateEmail = alternateEmail;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getCommunity() {
		return community;
	}

	public void setCommunity(String community) {
		this.community = community;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getMotherTongue() {
		return motherTongue;
	}

	public void setMotherTongue(String motherTongue) {
		this.motherTongue = motherTongue;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getSourceOfAdmission() {
		return sourceOfAdmission;
	}

	public void setSourceOfAdmission(String sourceOfAdmission) {
		this.sourceOfAdmission = sourceOfAdmission;
	}

	public String getDifferentlyAbled() {
		return differentlyAbled;
	}

	public void setDifferentlyAbled(String differentlyAbled) {
		this.differentlyAbled = differentlyAbled;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@JsonInclude(value = JsonInclude.Include.NON_NULL)
	public static class AcademicDetails {

		private AcademicDetail sslc;
		private AcademicDetail hsc;
		private AcademicDetail diploma;

		public AcademicDetail getSslc() {
			return sslc;
		}

		public void setSslc(AcademicDetail sslc) {
			this.sslc = sslc;
		}

		public AcademicDetail getHsc() {
			return hsc;
		}

		public void setHsc(AcademicDetail hsc) {
			this.hsc = hsc;
		}

		public AcademicDetail getDiploma() {
			return diploma;
		}

		public void setDiploma(AcademicDetail diploma) {
			this.diploma = diploma;
		}

	}

	@JsonInclude(value = JsonInclude.Include.NON_NULL)
	public static class CommunicationDetails {
		private Address communication;

		public Address getCommunication() {
			return communication;
		}

		public void setCommunication(Address communication) {
			this.communication = communication;
		}

	}

}