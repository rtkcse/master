/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package feedback.dao;

import java.util.List;

import feedback.common.Query;
import feedback.models.Document;

public interface CommonRepository<T extends Document> {
	<T> T create(Document document);

	<T> T update(String id, Document document);

	<T> List<T> get(Query querys, Class<T> documentClass);

	<T> T getById(String id, feedback.common.Query querys, Class<T> documentClass);

	void delete(String id, Class<T> documentClass);

}
