/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package feedback.dao;

import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import feedback.common.Query.ORDER;
import feedback.common.impl.QueryImpl;
import feedback.models.Document;
import feedback.models.Document.DELETE;

@Repository
public class CommonRepositoryImpl<T extends Document> implements CommonRepository<Document> {

	private static final String ID = "_id";

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public <T> T create(Document document) {
		document.setDel(DELETE.N);
		return (T) this.mongoTemplate.save(document);
	}

	@Override
	public <T> T update(String id, Document document) {
		document.setId(id);
		document.setDel(DELETE.N);
		return (T) this.mongoTemplate.save(document);
	}

	@Override
	public <T> List<T> get(feedback.common.Query querys, Class<T> documentClass) {
		Query query = new Query();
		query = this.toFilter(querys).with(toOrder(querys)).skip(querys.getStart()).limit(querys.getCount());
		query = this.valueToReturns(querys, query);
		return (List<T>) mongoTemplate.find(query, documentClass);
	}

	private Query valueToReturns(feedback.common.Query querys, Query query) {
		querys.getKeysToReturn().stream().forEach(key -> {
			query.fields().include(key);
		});
		return query;
	}

	@Override
	public <T> T getById(String id, feedback.common.Query querys, Class<T> documentClass) {
		Query query = new Query();
		query = this.valueToReturn(querys).addCriteria(toMongoId(id));
		return mongoTemplate.findOne(query, documentClass);
	}

	@Override
	public void delete(String id, Class<Document> documentClass) {
		Document currentDocument = (Document) this.getById(id, QueryImpl.builder().build(), documentClass);
		currentDocument.setDel(DELETE.Y);
		this.mongoTemplate.save(currentDocument);
	}

	private Sort toOrder(feedback.common.Query querys) {
		if (querys.getSortOrder() != null) {
			if (querys.getSortOrder().equals(ORDER.ASC)) {
				return Sort.by(Direction.ASC, querys.getSortBy());
			} else {
				return Sort.by(Direction.DESC, querys.getSortBy());
			}
		} else {
			return Sort.by(Direction.ASC, ID);
		}

	}

	private Criteria toMongoId(String id) {
		return Criteria.where(ID).is(id);
	}

	private Query toFilter(feedback.common.Query querys) {
		Query query = new Query();
		Set<Entry<String, Object>> filterQuery = querys.getFilters().entrySet();
		filterQuery.forEach(each -> {
			Criteria criteria = Criteria.where(each.getKey()).is(each.getValue());
			query.addCriteria(criteria);
		});
		query.addCriteria(Criteria.where("del").is(DELETE.N));
		return query;
	}

	private Query valueToReturn(feedback.common.Query querys) {
		Query query = new Query();
		querys.getKeysToReturn().stream().forEach(key -> {
			query.fields().include(key);
		});
		return query;
	}

}
