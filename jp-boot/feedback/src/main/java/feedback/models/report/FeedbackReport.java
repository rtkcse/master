/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package feedback.models.report;

import java.util.List;

import feedback.models.Document;

public class FeedbackReport extends Document {
	private String feedbackName;
	private String eventName;
	private List<FeedbackQuestion> questions;
	private String avgPole;
	private String feedbackTemplateId;
	private String referenceId;
	private String overAllAvg;
	private String polledUser;

	public String getPolledUser() {
		return polledUser;
	}

	public void setPolledUser(String polledUser) {
		this.polledUser = polledUser;
	}

	public String getOverAllAvg() {
		return overAllAvg;
	}

	public void setOverAllAvg(String overAllAvg) {
		this.overAllAvg = overAllAvg;
	}

	public String getFeedbackTemplateId() {
		return feedbackTemplateId;
	}

	public void setFeedbackTemplateId(String feedbackTemplateId) {
		this.feedbackTemplateId = feedbackTemplateId;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getFeedbackName() {
		return feedbackName;
	}

	public void setFeedbackName(String feedbackName) {
		this.feedbackName = feedbackName;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public List<FeedbackQuestion> getQuestions() {
		return questions;
	}

	public void setQuestions(List<FeedbackQuestion> questions) {
		this.questions = questions;
	}

	public String getAvgPole() {
		return avgPole;
	}

	public void setAvgPole(String avgPole) {
		this.avgPole = avgPole;
	}

	public static class Builder {
		FeedbackReport report = new FeedbackReport();

		public Builder questions(List<FeedbackQuestion> questions) {
			this.report.setQuestions(questions);
			return this;
		}

		public Builder avgPole(String avgPole) {
			this.report.setAvgPole(avgPole);
			return this;
		}

		public Builder polledUser(String polledUser) {
			this.report.setPolledUser(polledUser);
			return this;
		}

		public FeedbackReport build() {
			return this.report;
		}
	}

	public static class FeedbackQuestion {
		private String question;
		private String firstGrade;
		private String secondGrade;
		private String thirdGrade;
		private String fouthGrade;
		private String fifthGrade;
		private String avg;
		private String answer;
		private String TotalUser;

		public String getTotalUser() {
			return TotalUser;
		}

		public void setTotalUser(String totalUser) {
			TotalUser = totalUser;
		}

		public String getQuestion() {
			return question;
		}

		public void setQuestion(String question) {
			this.question = question;
		}

		public String getFirstGrade() {
			return firstGrade;
		}

		public void setFirstGrade(String firstGrade) {
			this.firstGrade = firstGrade;
		}

		public String getSecondGrade() {
			return secondGrade;
		}

		public void setSecondGrade(String secondGrade) {
			this.secondGrade = secondGrade;
		}

		public String getThirdGrade() {
			return thirdGrade;
		}

		public void setThirdGrade(String thirdGrade) {
			this.thirdGrade = thirdGrade;
		}

		public String getFouthGrade() {
			return fouthGrade;
		}

		public void setFouthGrade(String fouthGrade) {
			this.fouthGrade = fouthGrade;
		}

		public String getFifthGrade() {
			return fifthGrade;
		}

		public void setFifthGrade(String fifthGrade) {
			this.fifthGrade = fifthGrade;
		}

		public String getAvg() {
			return avg;
		}

		public void setAvg(String avg) {
			this.avg = avg;
		}

		public String getAnswer() {
			return answer;
		}

		public void setAnswer(String answer) {
			this.answer = answer;
		}

		public static FeedbackQuestionBuilder builder() {
			return new FeedbackQuestionBuilder();
		}

		public static class FeedbackQuestionBuilder {
			FeedbackQuestion question = new FeedbackQuestion();

			public FeedbackQuestionBuilder question(String question) {
				this.question.setQuestion(question);
				return this;
			}

			public FeedbackQuestionBuilder totalUser(String totalUser) {
				this.question.setTotalUser(totalUser);
				return this;
			}

			public FeedbackQuestionBuilder firstGrade(String firstGrade) {
				this.question.setFirstGrade(firstGrade);
				return this;
			}

			public FeedbackQuestionBuilder secondGrade(String secondGrade) {
				this.question.setSecondGrade(secondGrade);
				return this;
			}

			public FeedbackQuestionBuilder thirdGrade(String thirdGrade) {
				this.question.setThirdGrade(thirdGrade);
				return this;
			}

			public FeedbackQuestionBuilder fourthGrade(String fourthGrade) {
				this.question.setFouthGrade(fourthGrade);
				return this;
			}

			public FeedbackQuestionBuilder fifthGrade(String fifthGrade) {
				this.question.setFifthGrade(fifthGrade);
				return this;
			}

			public FeedbackQuestionBuilder avg(String avg) {
				this.question.setAvg(avg);
				return this;
			}

			public FeedbackQuestionBuilder answer(String answer) {
				this.question.setAnswer(answer);
				return this;
			}

			public FeedbackQuestion build() {
				return this.question;
			}
		}

	}
}
