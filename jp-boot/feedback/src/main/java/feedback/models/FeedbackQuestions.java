/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package feedback.models;

import feedback.custom.annotaions.DocumentName;

@DocumentName(collection = "feedbackQuestions")
public class FeedbackQuestions extends Document {
	private static final long serialVersionUID = 1L;
	// @NotNull(message = "ammount should not be empty")
	// @JsonIgnore
	private String question;
	private String answer;

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public FeedbackQuestions() {
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {
		FeedbackQuestions question = new FeedbackQuestions();

		public Builder id(String id) {
			this.question.setId(id);
			return this;
		}

		public Builder question(String question) {
			this.question.setQuestion(question);
			return this;
		}

		public Builder answer(String answers) {
			this.question.setAnswer(answers);
			return this;
		}

		public FeedbackQuestions build() {
			return this.question;
		}
	}

}