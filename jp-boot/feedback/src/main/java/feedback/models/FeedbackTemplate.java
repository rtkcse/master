/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package feedback.models;

import java.util.List;

import feedback.custom.annotaions.DocumentName;

@DocumentName(collection = "feedbackTemplate")
public class FeedbackTemplate extends Document<FeedbackTemplate> {
	private static final long serialVersionUID = 1L;
	// @NotNull(message = "ammount should not be empty")
	// @JsonIgnore
	private String name;
	private List<FeedbackQuestions> questions;

	public List<FeedbackQuestions> getQuestions() {
		return questions;
	}

	public void setQuestions(List<FeedbackQuestions> questions) {
		this.questions = questions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public FeedbackTemplate() {

	}

}