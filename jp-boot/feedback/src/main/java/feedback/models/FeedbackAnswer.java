/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package feedback.models;

import java.util.List;

import feedback.custom.annotaions.DocumentName;

@DocumentName(collection = "feedbackAnswer")
public class FeedbackAnswer extends Document<FeedbackAnswer> {
	private static final long serialVersionUID = 1L;
	// @NotNull(message = "ammount should not be empty")
	// @JsonIgnore
	private String name;
	private List<FeedbackQuestions> questions;
	private String referenceId;
	private String feedbackTemplateId;

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getFeedbackTemplateId() {
		return feedbackTemplateId;
	}

	public void setFeedbackTemplateId(String feedbackTemplateId) {
		this.feedbackTemplateId = feedbackTemplateId;
	}

	public List<FeedbackQuestions> getQuestions() {
		return questions;
	}

	public void setQuestions(List<FeedbackQuestions> questions) {
		this.questions = questions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public FeedbackAnswer() {
	}

}