/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package feedback.service;

import feedback.models.FeedbackAnswer;
import feedback.models.report.FeedbackReport;

public interface FeedbackAnswerService extends CommonService<FeedbackAnswer> {

	FeedbackReport eachFeedbackReport(String id);

}