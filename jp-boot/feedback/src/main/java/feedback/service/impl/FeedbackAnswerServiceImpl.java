/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package feedback.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feedback.common.Query;
import feedback.common.impl.QueryImpl;
import feedback.dao.CommonRepository;
import feedback.models.FeedbackAnswer;
import feedback.models.report.FeedbackReport;
import feedback.models.report.FeedbackReport.FeedbackQuestion;
import feedback.service.FeedbackAnswerService;

@Service
public class FeedbackAnswerServiceImpl extends AbstractServiceImpl<FeedbackAnswer> implements FeedbackAnswerService {

	@Autowired
	private CommonRepository<FeedbackAnswer> commonRepository;

	@Override
	public List<FeedbackAnswer> get(Query query) {
		return this.commonRepository.get(query, FeedbackAnswer.class);
	}

	@Override
	public FeedbackAnswer getById(String id, Query query) {
		return this.commonRepository.getById(id, query, FeedbackAnswer.class);
	}

	@Override
	public void deleteById(String id) {
		this.commonRepository.delete(id, FeedbackAnswer.class);
	}

	@Override
	public FeedbackReport eachFeedbackReport(String id) {
		List<FeedbackAnswer> feedbackAnswers = this.get(QueryImpl.builder().addFilter("referenceId", id).build());
		List<FeedbackReport.FeedbackQuestion> questions1 = new ArrayList<FeedbackReport.FeedbackQuestion>();
		feedbackAnswers.stream().forEach(data -> {
			data.getQuestions().forEach(eachQuestion -> {
				FeedbackReport.FeedbackQuestion question = new FeedbackReport.FeedbackQuestion();
				question.setQuestion(eachQuestion.getQuestion());
				question.setAnswer(eachQuestion.getAnswer());
				questions1.add(question);
			});
		});
		List<FeedbackReport> finalReports = new ArrayList<FeedbackReport>();
		FeedbackReport report = new FeedbackReport();
		report.setFeedbackTemplateId(feedbackAnswers.get(0).getFeedbackTemplateId());
		report.setReferenceId(feedbackAnswers.get(0).getReferenceId());
		report.setQuestions(questions1);

		calculateAnswerPerQuestions(report);
		report.setPolledUser(Integer.toString(feedbackAnswers.size()));

		return report;
	}

	private void calculateAnswerPerQuestions(FeedbackReport finalReport) {
		List<FeedbackQuestion> list = new ArrayList<FeedbackReport.FeedbackQuestion>();
		Set<String> questions = finalReport.getQuestions().stream().map(eachquestion -> eachquestion.getQuestion())
				.collect(Collectors.toSet());
		double totalques = questions.size();
		questions.stream().forEach(eachQuestion -> {
			FeedbackQuestion.FeedbackQuestionBuilder question = FeedbackQuestion.builder();
			List<FeedbackReport.FeedbackQuestion> answers = finalReport.getQuestions().stream()
					.filter(each -> each.getQuestion().equals(eachQuestion)).collect(Collectors.toList());
			double total = answers.stream().map(each -> Double.parseDouble(each.getAnswer())).reduce((double) 0,
					(a, b) -> a + b);
			double totalAns = answers.size();
			int firstGrade = 0;
			int secondGrade = 0;
			int thirdGrade = 0;
			int fourthGrade = 0;
			int fifthGrade = 0;
			for (FeedbackQuestion data : answers) {
				switch (data.getAnswer().trim()) {
				case "1":
					firstGrade++;
					break;
				case "2":
					secondGrade++;
					break;
				case "3":
					thirdGrade++;
					break;
				case "4":
					fourthGrade++;
					break;
				case "5":
					fifthGrade++;
					break;
				}
			}

			question.avg(String.format("%.1f", (total / totalAns))).firstGrade(Integer.toString(firstGrade))
					.secondGrade(Integer.toString(secondGrade)).thirdGrade(Integer.toString(thirdGrade))
					.fourthGrade(Integer.toString(fourthGrade)).fifthGrade(Integer.toString(fifthGrade))
					.question(eachQuestion);
			list.add(question.build());

		});
		double overAllAvg = list.stream().map(each -> Double.parseDouble(each.getAvg())).reduce((double) 0,
				(a, b) -> a + b);
		finalReport.setOverAllAvg(String.format("%.1f", overAllAvg / totalques));
		finalReport.setQuestions(list);
	}
}