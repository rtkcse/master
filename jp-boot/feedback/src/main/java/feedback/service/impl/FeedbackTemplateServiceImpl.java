/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package feedback.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feedback.common.Query;
import feedback.dao.CommonRepository;
import feedback.models.FeedbackTemplate;
import feedback.service.FeedbackTemplateService;

@Service
public class FeedbackTemplateServiceImpl extends AbstractServiceImpl<FeedbackTemplate>
		implements FeedbackTemplateService {

	@Autowired
	private CommonRepository<FeedbackTemplate> commonRepository;

	@Override
	public List<FeedbackTemplate> get(Query query) {
		return this.commonRepository.get(query, FeedbackTemplate.class);
	}

	@Override
	public FeedbackTemplate getById(String id, Query query) {
		return this.commonRepository.getById(id, query, FeedbackTemplate.class);
	}

	@Override
	public void deleteById(String id) {
		this.commonRepository.delete(id, FeedbackTemplate.class);
	}
}