/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package feedback.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import feedback.common.Query;
import feedback.dao.CommonRepository;
import feedback.models.FeedbackQuestions;
import feedback.service.FeedbackQuestionsService;

@Service
public class FeedbackQuestionsServiceImpl extends AbstractServiceImpl<FeedbackQuestions>
		implements FeedbackQuestionsService {

	@Autowired
	private CommonRepository<FeedbackQuestions> commonRepository;

	@Override
	public List<FeedbackQuestions> get(Query query) {
		return this.commonRepository.get(query, FeedbackQuestions.class);
	}

	@Override
	public FeedbackQuestions getById(String id, Query query) {
		return this.commonRepository.getById(id, query, FeedbackQuestions.class);
	}

	@Override
	public void deleteById(String id) {
		this.commonRepository.delete(id, FeedbackQuestions.class);
	}
}