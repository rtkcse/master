/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package feedback.service;

import feedback.models.FeedbackTemplate;

public interface FeedbackTemplateService extends CommonService<FeedbackTemplate> {

}