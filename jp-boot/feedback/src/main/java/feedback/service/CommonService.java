/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package feedback.service;

import java.util.List;

import feedback.common.Query;

public interface CommonService<T> {
	T create(T newDocument);

	List<T> get(Query query);

	T getById(String id, Query query);

	T update(String id, T newDocument);

	void deleteById(String id);
}
