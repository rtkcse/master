/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package feedback.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import feedback.common.SearchQueryImpl;
import feedback.custom.annotaions.BeanParam;
import feedback.models.FeedbackTemplate;
import feedback.service.FeedbackTemplateService;

@RestController
@RequestMapping(value = "/feedback/feedbackTemplate")
public class FeedbackTemplateController {
	@Autowired
	FeedbackTemplateService feedbackTemplateService;

	@PostMapping
	public FeedbackTemplate createTemplate(@Valid @RequestBody FeedbackTemplate feedbackTemplate) {
		return feedbackTemplateService.create(feedbackTemplate);
	}

	@GetMapping
	public List<FeedbackTemplate> getTemplate(@BeanParam SearchQueryImpl searchQuery) {
		return feedbackTemplateService.get(searchQuery.getBuilder().build());
	}

	@GetMapping("/{id}")
	public FeedbackTemplate getTemplateById(@PathVariable("id") String id, @BeanParam SearchQueryImpl searchQuery) {
		return feedbackTemplateService.getById(id, searchQuery.getBuilder().build());
	}

	@PutMapping("/{id}")
	public FeedbackTemplate updateTemplate(@PathVariable("id") String id,
			@Valid @RequestBody FeedbackTemplate feedbackTemplate) {
		return feedbackTemplateService.update(id, feedbackTemplate);
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable("id") String id) {
		this.feedbackTemplateService.deleteById(id);
	}
}
