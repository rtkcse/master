/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package feedback.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import feedback.common.SearchQueryImpl;
import feedback.custom.annotaions.BeanParam;
import feedback.models.FeedbackQuestions;
import feedback.service.FeedbackQuestionsService;

@RestController
@RequestMapping(value = "/feedback/questions")
public class FeedbackQuestionsController {
	@Autowired
	FeedbackQuestionsService feedbackQuestionsService;

	@PostMapping
	public FeedbackQuestions addQuestions(@Valid @RequestBody FeedbackQuestions feedbackQuestions) {
		return feedbackQuestionsService.create(feedbackQuestions);
	}

	@GetMapping
	public List<FeedbackQuestions> getQuestions(@BeanParam SearchQueryImpl searchQuery) {
		return feedbackQuestionsService.get(searchQuery.getBuilder().build());
	}

	@GetMapping("/{id}")
	public FeedbackQuestions getQuestionsById(@PathVariable("id") String id, @BeanParam SearchQueryImpl searchQuery) {
		return feedbackQuestionsService.getById(id, searchQuery.getBuilder().build());
	}

	@PutMapping("/{id}")
	public FeedbackQuestions updateQuestions(@PathVariable("id") String id,
			@Valid @RequestBody FeedbackQuestions feedbackQuestions) {
		return feedbackQuestionsService.update(id, feedbackQuestions);
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable("id") String id) {
		this.feedbackQuestionsService.deleteById(id);
	}
}
