/*
@Copyright (c) 2013-2014 JackProdigy.com All rights reserved
 */
package feedback.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import feedback.common.SearchQueryImpl;
import feedback.custom.annotaions.BeanParam;
import feedback.models.FeedbackAnswer;
import feedback.models.report.FeedbackReport;
import feedback.service.FeedbackAnswerService;

@RestController
@RequestMapping(value = "/feedback/answer")
public class FeedbackAnswerController {
	@Autowired
	FeedbackAnswerService feedbackAnswerService;

	@PostMapping
	public FeedbackAnswer createAnswer(@Valid @RequestBody FeedbackAnswer feedbackSurvey) {
		return feedbackAnswerService.create(feedbackSurvey);
	}

	@GetMapping
	public List<FeedbackAnswer> getAnswer(@BeanParam SearchQueryImpl searchQuery) {
		return feedbackAnswerService.get(searchQuery.getBuilder().build());
	}

	@GetMapping("/{id}")
	public FeedbackAnswer getAnswerById(@PathVariable("id") String id, @BeanParam SearchQueryImpl searchQuery) {
		System.out.println(id);
		return feedbackAnswerService.getById(id, searchQuery.getBuilder().build());
	}

	@GetMapping("/report")
	public FeedbackReport eachAnswer(@RequestParam("id") String id) {
		System.out.println(id);
		return feedbackAnswerService.eachFeedbackReport(id);

	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable("id") String id) {
		this.feedbackAnswerService.deleteById(id);
	}
}
